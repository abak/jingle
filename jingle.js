#!/usr/bin/gjs

const { GLib } = imports.gi;
const System = imports.system;

const APP_ID = 'Jingle';

GLib.set_prgname(APP_ID);

// The invocated program may be a symbolic link.
let entryPoint = System.programPath;
while (GLib.file_test(entryPoint, GLib.FileTest.IS_SYMLINK))
    entryPoint = GLib.file_read_link(entryPoint);

const dir = GLib.path_get_dirname(entryPoint);
const srcdir = GLib.build_filenamev([dir, 'src']);

imports.searchPath.unshift(srcdir);
const Main = imports.main;
imports.searchPath.shift();

Main.main([System.programInvocationName].concat(ARGV), null);
