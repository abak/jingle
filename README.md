# Jingle
A user-unfriendly GNOME Glade substitute.

Jingle is based on [Gladiator](https://codeberg.org/som/Gladiator) and [Evaluator](https://codeberg.org/som/Evaluator).

It is mainly a hack of the [GTK Inspector](https://wiki.gnome.org/Projects/GTK/Inspector).

## Setup
Initialize Gladiator and Evaluator (optional) submodules:

```sh
git submodule update --init
```

### Run
Jingle is launchable without any installation:

```sh
gjs jingle.js [--id] [--no-header-bar] [--type] [FILE…]
```

### Install
```sh
meson build && cd build && ninja && sudo ninja install
```

### Uninstall
```sh
cd build && sudo ninja uninstall
```

## Usage
Open the GTK Inspector with `Ctrl+D` or `Ctrl+I`.

### Builder files
You can pass builder files to start the application with, otherwise the window will be an empty playground.

Jingle will search a toplevel widget:

1. An object with the id provided by the `--id` option.
1. An object with the id "jingle-widget".
2. Otherwise a window.
3. Otherwise a random toplevel widget.

If the toplevel widget is not a window, it will be the window child.

### Classes registration
JavaScript developers can also pass JS files to register custom GObject classes, to be consumed by the builder files as well as the in-app packing feature.

The classes have to be simply registered with `GObject.registerClass`. See the [example](./tests/register).

### Window type

Alternatively to builder files, the `--type` option lets you specify a type name for the window:

```sh
gjs jingle.js --type AdwPreferencesWindow
```

### Gladiator
Gladiator is integrated in the first page ("Object" > "Show Details"):

* The "Add child" button lets you add an UI child to the current object, where "UI child" does not necessarily mean "widget child".
* The "Remove" button lets you remove the current object from its UI parent, where "UI parent" does not necessarily mean "widget parent".
* Finally, the "UI" page likely displays the UI definition of the current object.

![Gladiator integration](data/screenshots/gladiator-integration.png)

Additionally:

* The "Parent widget" button is extended to pass over undefinable widgets on right click.
* The "First child" button is extended to pass over undefinable widgets on right click.
* The "Controllers" page is extended with addition and removal capacities.
* The "Size Groups" page is extended with addition and removal capacities.
* A "Constraints" page is added for constraint layouts.

### Evaluator
Evaluator has its own toplevel page.

Some special objects are exposed:

* The active window is gettable with `jwin`.
* The current object is gettable with `jobj`. The current object is the one that is inspected in the first page of the inspector and handled by the Gladiator tool. Note that it may be null because Gladiator only handle the objects that are usefull for the UI definition (e.g. middle containers of complex widgets are ignored).
* If the application has received builder files, the `GtkBuilder` instance is retrievable with `jbuilder`.
* If the application has received JS files, the registered classes are retrievable with `jclasses`.

For more informations, see [Evaluator](./Evaluator/README.md).
