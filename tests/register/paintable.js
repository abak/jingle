// SPDX-FileCopyrightText: 1997-2021 The GTK Team
// SPDX-FileCopyrightText: 2021 Abakkk
// SPDX-License-Identifier: GPL-3.0-or-later
// From GTK Demo - https://gitlab.gnome.org/GNOME/gtk/tree/master/demos/gtk-demo/paintable.c

const { Gdk, GObject, Graphene } = imports.gi;

const RADIUS = 0.3;

GObject.registerClass({
    GTypeName: 'NuclearIcon',
    Implements: [Gdk.Paintable],
}, class extends GObject.Object {
    vfunc_get_flags() {
        return Gdk.PaintableFlags.CONTENTS | Gdk.PaintableFlags.SIZE;
    }

    vfunc_snapshot(snapshot, width, height) {
        snapshot.append_color(
            new Gdk.RGBA({ red: 0.9, green: 0.75, blue: 0.15, alpha: 1.0 }),
            new Graphene.Rect({ size: { width, height } })
        );

        let min = Math.min(width, height);
        let cr = snapshot.append_cairo(
            new Graphene.Rect().init((width - min) / 2, (height - min) / 2, min, min)
        );

        cr.translate(width / 2, height / 2);
        cr.scale(min, min);

        cr.arc(0, 0, 0.1, -Math.PI, Math.PI);
        cr.fill();

        cr.setLineWidth(RADIUS);
        cr.setDash([RADIUS * Math.PI / 3], 0);
        cr.arc(0, 0, RADIUS, -Math.PI, Math.PI);
        cr.stroke();

        cr.$dispose();
    }
});
