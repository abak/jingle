// SPDX-FileCopyrightText: 2021 Abakkk
// SPDX-License-Identifier: GPL-3.0-or-later

const {  GObject, Gtk } = imports.gi;

GObject.registerClass({
    GTypeName: 'NuclearDialog',
    Properties: {
        'code': GObject.ParamSpec.string(
            'code', "Code", "The nuclear code",
            GObject.ParamFlags.READWRITE, null
        ),
    },
}, class extends Gtk.Dialog {
    _init(params) {
        super._init(Object.assign({
            title: "Code",
            hideOnClose: true,
            destroyWithParent: true,
            modal: true,
        }, params));

        let handler = this.connect('map', () => {
            this.disconnect(handler);

            this.set_response_sensitive(Gtk.ResponseType.APPLY, false);

            let entry = this.get_content_area().get_first_child();
            entry.connect('changed', () => {
                this.set_response_sensitive(Gtk.ResponseType.APPLY, entry.text == this.code);
            });
        });
    }

    vfunc_response(responseId) {
        this.close();

        if (responseId == Gtk.ResponseType.APPLY)
            this.transientFor.destroy();
    }
});

GObject.registerClass({
    GTypeName: 'NuclearButton',
}, class extends Gtk.Button {
    vfunc_clicked() {
        let dialog = Gtk.Window.list_toplevels().find(window => window.constructor.$gtype.name == 'NuclearDialog');
        dialog.show();
    }
});
